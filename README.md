# rdb #

RustDB (rdb) is a simple database implemented in Rust.

## Data Types

rdb currently supports 3 different data types:

- Long
- Boolean
- String

Each data type is stored as 12 bytes:

- First byte indicates the data type itself:
  - `l` = Long
  - `b` = Boolean
  - `s` = String
- Last 11 bytes is the data  

## Row

The `Row` struct is a wrapper around `DataType` structs.

## Table

The `Table` struct is a wrapper around `Row` structs and has associated `Metadata`.

## Metadata

The table metadata is used to define the columns withing the table.

Each metadata is stored as 12 bytes:

- First byte indicates the data type of the column:
  - `l` = Long
  - `b` = Boolean
  - `s` = String
- Second byte is a colon `:`
- Last 10 bytes is the column name