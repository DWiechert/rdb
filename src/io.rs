use std::fs::{File, OpenOptions};
use std::io::{Write, Read};

use crate::table::Table;

pub struct FileManager {
    filename: String,
    file: File,
}

impl FileManager {
    pub fn new(name: &String) -> Self {
        let file_result = OpenOptions::new()
            .read(true)
            .write(true)
            .append(true)
            .create(true) // Create file if it doesn't exist
            .open(name);

        if file_result.is_ok() {
            Self {
                filename: name.clone(),
                file: file_result.unwrap(),
            }
        } else {
            panic!("Unable to open file: {}", name);
        }
    }

    pub fn write_table(&mut self, table: &Table) -> bool {
        let bytes = table.to_bytes();
        let result = self.file.write_all(bytes.as_ref());
        return match result {
            Ok(_) => true,
            Err(e) => {
                eprintln!("Error writing table: {}", e);
                false
            }
        };
    }

    pub fn read_table(&mut self) -> Table {
        let mut bytes: Vec<u8> = Vec::new();
        let content = self.file.read_to_end(&mut bytes);
        return match content {
            Ok(_) => Table::from_bytes(bytes),
            Err(e) => {
                panic!("Error reading table: {}", e)
            }
        }
    }
}

/// Tests for FileManager
#[cfg(test)]
mod tests {
    use std::path::PathBuf;
    use std::time::SystemTime;

    use crate::data::{Data, DataType};
    use crate::io::FileManager;
    use crate::row::Row;
    use crate::table::{Metadata, Table};

    #[test]
    fn new_file_manager_new_file() {
        let start = SystemTime::now();

        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("resources");
        path.push("test");
        path.push("new_file.txt");

        let filename = path.as_os_str().to_str().unwrap().to_string();
        let fm = FileManager::new(&filename);

        assert_eq!(filename, fm.filename);
        let metadata = fm.file.metadata().unwrap();
        assert_eq!(true, metadata.is_file());
        // Ensure file was created after test started
        // FIXME: Currently, this fails in Docker
        // creation time is not available on this platform currently
        // assert_eq!(start, start.min(metadata.created().unwrap()));

        // Delete file after execution
        // Note: This will not run if the test fails
        // Currently, there is no easy way to implement this:
        // https://stackoverflow.com/q/59077877/864369
        let remove = std::fs::remove_file(path.clone());
        if remove.is_err() {
            eprintln!("Could not remove test file: {}", path.as_os_str().to_str().unwrap());
        }
    }

    #[test]
    fn new_file_manager_existing_file() {
        let start = SystemTime::now();

        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("resources");
        path.push("test");
        path.push("existing_file.txt");

        let filename = path.as_os_str().to_str().unwrap().to_string();
        let fm = FileManager::new(&filename);

        assert_eq!(filename, fm.filename);
        let metadata = fm.file.metadata().unwrap();
        assert_eq!(true, metadata.is_file());
        // Ensure file was created before test started
        assert_eq!(start, start.max(metadata.created().unwrap()));
    }

    #[test]
    fn write_table() {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("resources");
        path.push("test");
        path.push("write_table.bin");

        let filename = path.as_os_str().to_str().unwrap().to_string();
        let mut fm = FileManager::new(&filename);

        assert_eq!(filename, fm.filename);
        let metadata = fm.file.metadata().unwrap();
        assert_eq!(true, metadata.is_file());

        let bd1 = Data::new(DataType::Boolean, [0x01].to_vec());
        let ld1 = Data::new(DataType::Long, [0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec());
        let sd1 = Data::new(DataType::String, [0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row1 = Row::new(vec![bd1, ld1, sd1]);
        let bd2 = Data::new(DataType::Boolean, [0x00].to_vec());
        let ld2 = Data::new(DataType::Long, [0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x39].to_vec());
        let sd2 = Data::new(DataType::String, [0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row2 = Row::new(vec![bd2, ld2, sd2]);

        let ct = vec![DataType::Boolean, DataType::Long, DataType::String];
        let mut cn: Vec<[u8; 10]> = Vec::new();

        let mut c1 = [0 as u8; 10];
        for (index, c) in "column1".chars().enumerate() {
            c1[index] = c as u8;
        }
        cn.push(c1);

        let mut c2 = [0 as u8; 10];
        for (index, c) in "c2".chars().enumerate() {
            c2[index] = c as u8;
        }
        cn.push(c2);

        let mut c3 = [0 as u8; 10];
        for (index, c) in "my_string".chars().enumerate() {
            c3[index] = c as u8;
        }
        cn.push(c3);

        let m = Metadata::new(ct, cn);
        let table = Table::new(m, vec![row1, row2]);

        fm.write_table(&table);

        let fs_meta = fm.file.metadata().unwrap();
        assert_eq!(113, fs_meta.len());

        let remove = std::fs::remove_file(path.clone());
        if remove.is_err() {
            eprintln!("Could not remove test file: {}", path.as_os_str().to_str().unwrap());
        }
    }

    #[test]
    fn read_table() {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("resources");
        path.push("test");
        path.push("read_table.bin");

        let filename = path.as_os_str().to_str().unwrap().to_string();
        let mut fm = FileManager::new(&filename);

        assert_eq!(filename, fm.filename);
        let metadata = fm.file.metadata().unwrap();
        assert_eq!(true, metadata.is_file());

        let table = fm.read_table();

        let bd1 = Data::new(DataType::Boolean, [0x01].to_vec());
        let ld1 = Data::new(DataType::Long, [0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec());
        let sd1 = Data::new(DataType::String, [0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row1 = Row::new(vec![bd1, ld1, sd1]);
        let bd2 = Data::new(DataType::Boolean, [0x00].to_vec());
        let ld2 = Data::new(DataType::Long, [0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x39].to_vec());
        let sd2 = Data::new(DataType::String, [0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row2 = Row::new(vec![bd2, ld2, sd2]);

        let ct = vec![DataType::Boolean, DataType::Long, DataType::String];
        let mut cn: Vec<[u8; 10]> = Vec::new();

        let mut c1 = [0 as u8; 10];
        for (index, c) in "column1".chars().enumerate() {
            c1[index] = c as u8;
        }
        cn.push(c1);

        let mut c2 = [0 as u8; 10];
        for (index, c) in "c2".chars().enumerate() {
            c2[index] = c as u8;
        }
        cn.push(c2);

        let mut c3 = [0 as u8; 10];
        for (index, c) in "my_string".chars().enumerate() {
            c3[index] = c as u8;
        }
        cn.push(c3);

        let m = Metadata::new(ct, cn);
        let expected_table = Table::new(m, vec![row1, row2]);

        assert_eq!(expected_table, table);
    }
}