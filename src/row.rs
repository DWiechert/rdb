use crate::data::Data;

/// Definition for a row within rdb. It contains
/// a vector of all of the cells.
#[derive(Debug)]
#[derive(PartialEq)]
pub struct Row {
    values: Vec<Data>
}

impl Row {
    pub fn new(values: Vec<Data>) -> Self {
        Self {
            values
        }
    }

    /// Converts a `Row` to a byte vector.
    pub fn to_bytes(&self) -> Vec<[u8; 12]> {
        let mut v: Vec<[u8; 12]> = Vec::new();
        for value in &self.values {
            v.push(value.to_bytes());
        }
        v
    }

    /// Converts a byte vector to a `Row`.
    pub fn from_bytes(bytes: Vec<[u8; 12]>) -> Row {
        let mut r: Vec<Data> = Vec::new();
        for b in bytes {
            let data = Data::from_bytes(b);
            r.push(data);
        }
        Row{
            values: r
        }
    }

    /// Converts a `Row` to a string.
    pub fn to_string(&self) -> String {
        let mut s: Vec<String> = Vec::new();
        for value in &self.values {
            s.push(value.to_string());
        }
        s.join(",")
    }
}

/// Tests for Row.
#[cfg(test)]
mod tests {
    use crate::data::{Data, DataType};
    use crate::row::Row;

    /// Ensures row -> byte vector works as expected.
    #[test]
    fn row_to_bytes() {
        let bd = Data::new(DataType::Boolean,[0x01].to_vec());
        let ld = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec());
        let sd = Data::new(DataType::String,[0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());

        let row = Row{
            values: vec![bd, ld, sd]
        };

        let actual_bytes = row.to_bytes();
        assert_eq!(3, actual_bytes.len());

        let mut expected_bd = [0 as u8; 12];
        expected_bd[0] = 'b' as u8;
        expected_bd[1] = 0x01;
        assert_eq!(&expected_bd, actual_bytes.get(0).unwrap());

        let expected_ld = ['l' as u8, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x00, 0x00, 0x00];
        assert_eq!(&expected_ld, actual_bytes.get(1).unwrap());

        let expected_sd = ['s' as u8, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64];
        assert_eq!(&expected_sd, actual_bytes.get(2).unwrap());
    }

    /// Ensure byte vector -> row works as expected.
    #[test]
    fn bytes_to_row() {
        let mut bd_bytes = [0 as u8; 12];
        bd_bytes[0] = 'b' as u8;
        bd_bytes[1] = 0x01;
        let ld_bytes = ['l' as u8, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x00, 0x00, 0x00];
        let sd_bytes = ['s' as u8, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64];
        let v = vec![ld_bytes, sd_bytes, bd_bytes];

        let row = Row::from_bytes(v);
        assert_eq!(3, row.values.len());

        let expected_ld = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec());
        assert_eq!(&expected_ld, row.values.get(0).unwrap());

        let expected_sd =  Data::new(DataType::String,[0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        assert_eq!(&expected_sd, row.values.get(1).unwrap());

        let expected_bd = Data::new(DataType::Boolean,[0x01].to_vec());
        assert_eq!(&expected_bd, row.values.get(2).unwrap());
    }

    /// Ensures row -> string works as expected.
    #[test]
    fn row_to_string() {
        let bd = Data::new(DataType::Boolean,[0x01].to_vec());
        let ld = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec());
        let sd = Data::new(DataType::String,[0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());

        let row = Row{
            values: vec![bd, ld, sd]
        };

        let actual_string = row.to_string();
        let expected_string = "true,12345678,Hello World".to_string();
        assert_eq!(expected_string, actual_string);
    }
}