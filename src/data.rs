/// Different data type that can be represented in rdb.
#[derive(Debug)]
#[derive(PartialEq)]
pub enum DataType {
    Long,
    Boolean,
    String
}

/// Lowest type of data rdb will work with. It contains both the metadata
/// and the actual data for the cell.
#[derive(Debug)]
#[derive(PartialEq)]
pub struct Data {
    data_type: DataType,
    data: Vec<u8>
}

impl Data {
    pub fn new(data_type: DataType, data: Vec<u8>) -> Self {
        Self {
            data_type,
            data
        }
    }

    /// Converts a `Data` to a byte array.
    pub fn to_bytes(&self) -> [u8; 12] {
        let mut bytes = [0 as u8; 12];
        match self.data_type {
            DataType::Long => {
                let b = self.data.as_slice();
                bytes[0] = 'l' as u8;
                bytes[1] = b[0];
                bytes[2] = b[1];
                bytes[3] = b[2];
                bytes[4] = b[3];
                bytes[5] = b[4];
                bytes[6] = b[5];
                bytes[7] = b[6];
                bytes[8] = b[7];
            }
            DataType::Boolean => {
                let b = self.data.as_slice();
                bytes[0] = 'b' as u8;
                bytes[1] = b[0];
            }
            DataType::String => {
                let b = self.data.as_slice();
                bytes[0] = 's' as u8;
                bytes[1] = b[0];
                bytes[2] = b[1];
                bytes[3] = b[2];
                bytes[4] = b[3];
                bytes[5] = b[4];
                bytes[6] = b[5];
                bytes[7] = b[6];
                bytes[8] = b[7];
                bytes[9] = b[8];
                bytes[10] = b[9];
                bytes[11] = b[10];
            }
        }
        bytes
    }

    /// Converts a byte array to a `Data`.
    pub fn from_bytes(bytes: [u8; 12]) -> Data {
        let dt = bytes[0];
        let mut d: Vec<u8> = vec![];
        match dt as char {
            'l' => {
                d.push(bytes[1]);
                d.push(bytes[2]);
                d.push(bytes[3]);
                d.push(bytes[4]);
                d.push(bytes[5]);
                d.push(bytes[6]);
                d.push(bytes[7]);
                d.push(bytes[8]);
                Data {
                    data_type: DataType::Long,
                    data: d.clone()
                }
            }
            'b' => {
                d.push(bytes[1]);
                Data {
                    data_type: DataType::Boolean,
                    data: d.clone()
                }
            }
            's' => {
                d.push(bytes[1]);
                d.push(bytes[2]);
                d.push(bytes[3]);
                d.push(bytes[4]);
                d.push(bytes[5]);
                d.push(bytes[6]);
                d.push(bytes[7]);
                d.push(bytes[8]);
                d.push(bytes[9]);
                d.push(bytes[10]);
                d.push(bytes[11]);
                Data {
                    data_type: DataType::String,
                    data: d.clone()
                }
            }
            _ => {
                eprintln!("Unknown data type: {}", dt);
                panic!()
            }
        }
    }

    /// Converts a `Data` to a string.
    pub fn to_string(&self) -> String {
        match self.data_type {
            DataType::Boolean => {
                let t: u8 = 1;
                let tr: &u8 = &t;
                if tr == self.data.get(0).unwrap() {
                    String::from("true")
                } else {
                    String::from("false")
                }
            }
            _ => {
                String::from_utf8(self.data.clone()).unwrap()
            }
        }
    }
}

/// Tests for Data.
#[cfg(test)]
mod tests {
    use crate::data::{Data, DataType};

    /// Ensures long data -> byte array works as expected.
    #[test]
    fn long_to_bytes() {
        let b: Vec<u8> = [0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec();

        let expected_data = Data {
            data_type: DataType::Long,
            data: b
        };
        let expected_bytes: [u8; 12] = ['l' as u8,
            0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
            0, 0, 0
        ];

        let actual_bytes = expected_data.to_bytes();
        assert_eq!(expected_bytes, actual_bytes);
    }

    /// Ensures byte array -> long data works as expected.
    #[test]
    fn long_from_bytes() {
        let b: Vec<u8> = [0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec();

        let expected_data = Data {
            data_type: DataType::Long,
            data: b
        };
        let expected_bytes: [u8; 12] = ['l' as u8,
            0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
            0, 0, 0
        ];

        let actual_data = Data::from_bytes(expected_bytes);
        assert_eq!(expected_data.data_type, actual_data.data_type);
        let new = [0 as u8; 0];
        let v: Vec<u8> = expected_bytes.to_vec().splice(1..9, new.iter().cloned()).collect();
        assert_eq!(v, actual_data.data);
    }

    /// Ensures long data -> string works as expected.
    #[test]
    fn long_to_string() {
        let b: Vec<u8> = [0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec();

        let expected_data = Data {
            data_type: DataType::Long,
            data: b
        };

        assert_eq!("12345678", expected_data.to_string());
    }

    /// Ensures boolean data -> byte array works as expected.
    #[test]
    fn boolean_to_bytes() {
        let b: Vec<u8> = [0x01].to_vec();

        let expected_data = Data {
            data_type: DataType::Boolean,
            data: b
        };
        let expected_bytes: [u8; 12] = ['b' as u8,
            0x01,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ];

        let actual_bytes = expected_data.to_bytes();
        assert_eq!(expected_bytes, actual_bytes);
        assert_eq!("true", expected_data.to_string());
    }

    /// Ensures byte array -> boolean data works as expected.
    #[test]
    fn boolean_from_bytes() {
        let b: Vec<u8> = [0x01].to_vec();

        let expected_data = Data {
            data_type: DataType::Boolean,
            data: b
        };
        let expected_bytes: [u8; 12] = ['b' as u8,
            0x01,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ];

        let actual_data = Data::from_bytes(expected_bytes);
        assert_eq!(expected_data.data_type, actual_data.data_type);
        let new = [0 as u8; 0];
        let v: Vec<u8> = expected_bytes.to_vec().splice(1..2, new.iter().cloned()).collect();
        assert_eq!(v, actual_data.data);
    }

    /// Ensures boolean true data -> string works as expected.
    #[test]
    fn boolean_true_to_string() {
        let b: Vec<u8> = [0x01].to_vec();

        let expected_data = Data {
            data_type: DataType::Boolean,
            data: b
        };

        assert_eq!("true", expected_data.to_string());
    }

    /// Ensures boolean false data -> string works as expected.
    #[test]
    fn boolean_false_to_string() {
        let b: Vec<u8> = [0x00].to_vec();

        let expected_data = Data {
            data_type: DataType::Boolean,
            data: b
        };

        assert_eq!("false", expected_data.to_string());
    }

    /// Ensures string data -> byte array works as expected.
    #[test]
    fn string_to_bytes() {
        let b: Vec<u8> = [0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec();

        let expected_data = Data {
            data_type: DataType::String,
            data: b
        };
        let expected_bytes: [u8; 12] = ['s' as u8,
            0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64
        ];

        let actual_bytes = expected_data.to_bytes();
        assert_eq!(expected_bytes, actual_bytes);
    }

    /// Ensures byte array -> string data works as expected.
    #[test]
    fn string_from_bytes() {
        let b: Vec<u8> = [0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec();

        let expected_data = Data {
            data_type: DataType::String,
            data: b
        };
        let expected_bytes: [u8; 12] = ['s' as u8,
            0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64
        ];

        let actual_data = Data::from_bytes(expected_bytes);
        assert_eq!(expected_data.data_type, actual_data.data_type);
        let new = [0 as u8; 0];
        let v: Vec<u8> = expected_bytes.to_vec().splice(1..12, new.iter().cloned()).collect();
        assert_eq!(v, actual_data.data);
    }

    /// Ensures string data -> string works as expected.
    #[test]
    fn string_to_string() {
        let b: Vec<u8> = [0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec();

        let expected_data = Data {
            data_type: DataType::String,
            data: b
        };

        assert_eq!("Hello World", expected_data.to_string());
    }
}